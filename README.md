Gurtam test app

Server: python tornado web framework for handling POI(point of interest); actions: CRUD.

Client: single page with map Leaflet(http://leafletjs.com/) without js libs.

Usage:

* clone repo
* make file executable: chmod +x server.py
* run server: ./server

API calls:

* /get_pois {} - returns dict with pois list
* /add_poi {"id": <id>, "name": <name>, "latitude": <latitude>, "longitude": <longitude>} - adds new poi
* /update_poi {"id": <id>, "latitude": <latitude>, "longitude": <longitude>} - updates poi's coords
* /delete_poi {"id": <id>} - deletes poi