var names = ["Кафе", "Отель", "WC", "Парковка", "Памятник"];
var mymap;

window.onload = function () {
  mymap = L.map("map").setView([53.896507, 27.547596], 12);
  mymap.doubleClickZoom.disable();

  L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw", {
    maxZoom: 18,
    attribution: "Map data &copy; <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors, " +
      "<a href='http://creativecommons.org/licenses/by-sa/2.0/'>CC-BY-SA</a>, " +
      "Imagery © <a href='http://mapbox.com'>Mapbox</a>",
    id: "mapbox.streets"
  }).addTo(mymap);

  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "get_pois", true);
  xhttp.send();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var data = JSON.parse(this.responseText);
      for (i = 0; i < data.pois.length; i++) {
        var marker = L.marker([data.pois[i].latitude, data.pois[i].longitude], {draggable: "true"}).addTo(mymap);
        marker.bindPopup(data.pois[i].name);
        marker._icon.id = data.pois[i].id;
        marker.on("dragend", update_marker);
        marker.on("dblclick", delete_marker);
      }
    }
  }

  mymap.on('dblclick', function(e) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "add_poi", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    var name = names[Math.floor(Math.random() * names.length)];
    var id = Math.random().toString(36).substr(2, 9);
    xhttp.send(JSON.stringify({"name": name,
                               "id": id,
                               "latitude": e.latlng.lat,
                               "longitude": e.latlng.lng}));
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var marker = L.marker([e.latlng.lat, e.latlng.lng], {draggable: "true"}).addTo(mymap);
        marker.bindPopup(name);
        marker._icon.id = id;
        marker.on("dragend", update_marker);
        marker.on("dblclick", delete_marker);
      }
    }
  });
}

function delete_marker() {
  var temp = this;
  var xhttp = new XMLHttpRequest();
  xhttp.open("DELETE", "delete_poi", true);
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.send(JSON.stringify({"id": temp._icon.id}));
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      mymap.removeLayer(temp);
    }
  }
}

function update_marker() {
  var position = this.getLatLng();
  var id = this._icon.id;
  var xhttp = new XMLHttpRequest();
  xhttp.open("PUT", "update_poi", true);
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.send(JSON.stringify({"id": id, "latitude": position.lat, "longitude": position.lng}));
}
