#!/usr/bin/python
import tornado.ioloop
import tornado.web
import tornado.template
import tornado.gen
import signal
import json
import os


filename = "pois.json"


def read_json():
    with open(filename, "r") as f:
        data = json.load(f)
    return data


def write_json(data):
    with open(filename, "w+") as f:
        json.dump(data, f, indent=4)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class PoiHandler(tornado.web.RequestHandler):
    @tornado.gen.coroutine
    def get(self):
        result = yield self.load_json()
        self.write(json.dumps(result))

    @tornado.gen.coroutine
    def load_json(self):
        return read_json()

    @tornado.gen.coroutine
    def post(self):
        result = yield self.add_to_json()

    @tornado.gen.coroutine
    def add_to_json(self):
        json_data = json.loads(self.request.body)
        id = json_data["id"]
        name = json_data["name"]
        latitude = json_data["latitude"]
        longitude = json_data["longitude"]
        data = read_json()
        data["pois"].append({"id": id, "name": name, "latitude": latitude, "longitude": longitude})
        write_json(data)
        return None

    @tornado.gen.coroutine
    def delete(self):
        result = yield self.delete_from_json()

    @tornado.gen.coroutine
    def delete_from_json(self):
        json_data = json.loads(self.request.body)
        id = json_data["id"]
        data = read_json()
        data["pois"].remove(filter(lambda x: x["id"] == id, data["pois"])[0])
        write_json(data)
        return None

    @tornado.gen.coroutine
    def put(self):
        result = yield self.update_json()

    @tornado.gen.coroutine
    def update_json(self):
        json_data = json.loads(self.request.body)
        id = json_data["id"]
        latitude = json_data["latitude"]
        longitude = json_data["longitude"]
        data = read_json()
        poi = filter(lambda x: x["id"] == id, data["pois"])[0]
        poi["latitude"] = latitude
        poi["longitude"] = longitude
        write_json(data)
        return None


def signal_handler(signum, frame):
    tornado.ioloop.IOLoop.instance().stop()


if __name__ == "__main__":
    if not os.path.exists(filename):
        with open(filename, "w+") as f:
            json.dump({"pois": []}, f, indent=4)
    webapp = tornado.web.Application([
        (r"/", MainHandler),
        (r"/get_pois", PoiHandler),
        (r"/add_poi", PoiHandler),
        (r"/delete_poi", PoiHandler),
        (r"/update_poi", PoiHandler),
        (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "static"}),
    ], template_path="templates/", debug=True)
    webapp.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
    signal.signal(signal.SIGINT, signal_handler)
